package com.richard.bancointer.bancointertestapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BancoInterTestApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
