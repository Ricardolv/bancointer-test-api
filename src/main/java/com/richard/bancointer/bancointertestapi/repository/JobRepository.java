package com.richard.bancointer.bancointertestapi.repository;

import com.richard.bancointer.bancointertestapi.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {



}
