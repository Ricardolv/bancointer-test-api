package com.richard.bancointer.bancointertestapi.repository;


import com.richard.bancointer.bancointertestapi.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    List<Task> findByCreatedAt(LocalDate createdAt);

}
