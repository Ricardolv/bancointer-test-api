package com.richard.bancointer.bancointertestapi.repository;

import com.richard.bancointer.bancointertestapi.model.ParentJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ParentJobRepository extends JpaRepository<ParentJob, Integer> {
}
