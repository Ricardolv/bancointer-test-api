package com.richard.bancointer.bancointertestapi.model;

import com.richard.bancointer.bancointertestapi.dto.JobDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Data
@Entity
@DynamicUpdate
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean active;

    @OneToMany(mappedBy="job", fetch = FetchType.LAZY)
    private List<Task> tasks = new ArrayList<>();

    @OneToOne(mappedBy="job", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ParentJob parentJob;


    public static Job fromDTO(JobDto objDto) {
        Job job = new Job();

        BeanUtils.copyProperties(objDto, job, "id", "tasks", "parentJob");
        return job;
    }
}
