package com.richard.bancointer.bancointertestapi.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.richard.bancointer.bancointertestapi.dto.TaskDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.time.LocalDate;


@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Data
@Entity
@DynamicUpdate
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Long weight;
    private boolean completed;
    private LocalDate createdAt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="job_id")
    private Job job;


    public static Task fromDTO(TaskDto objDto) {
        Task task = new Task();
        BeanUtils.copyProperties(objDto, task, "id");
        return task;
    }
}
