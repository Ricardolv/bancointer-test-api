package com.richard.bancointer.bancointertestapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.richard.bancointer.bancointertestapi.dto.JobDto;
import com.richard.bancointer.bancointertestapi.dto.ParentJobDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Data
@Entity
@DynamicUpdate
public class ParentJob {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean active;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="Job_id")
    private Job job;


    public static ParentJob fromDTO(ParentJobDto objDto) {
        ParentJob job = new ParentJob();

        BeanUtils.copyProperties(objDto, job, "id");
        return job;
    }

}
