package com.richard.bancointer.bancointertestapi.resources;

import com.richard.bancointer.bancointertestapi.dto.JobDto;
import com.richard.bancointer.bancointertestapi.dto.TaskDto;
import com.richard.bancointer.bancointertestapi.model.Job;
import com.richard.bancointer.bancointertestapi.model.ParentJob;
import com.richard.bancointer.bancointertestapi.model.Task;
import com.richard.bancointer.bancointertestapi.repository.ParentJobRepository;
import com.richard.bancointer.bancointertestapi.resources.exception.StandardError;
import com.richard.bancointer.bancointertestapi.services.JobService;
import com.richard.bancointer.bancointertestapi.services.ParentJobService;
import com.richard.bancointer.bancointertestapi.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value="/jobs")

public class JobResource {

    @Autowired
    private JobService service;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ParentJobService parentJobService;

    @GetMapping
    public ResponseEntity<List<Job>> findByDate(@RequestParam(name = "sortByWeight") Boolean sortByWeight) {
        List<Job> list = service.sortByWeight(sortByWeight);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity<List<Job>> create(@Valid @RequestBody JobDto objDto) {

        Job obj = Job.fromDTO(objDto);
        ParentJob parentJob = ParentJob.fromDTO(objDto.getParentJob());
        obj.setParentJob(parentJob);

        obj = service.insert(obj);

        parentJob.setJob(obj);
        parentJobService.insert(parentJob);

        for (TaskDto dto : objDto.getTasks()) {
            Task task = Task.fromDTO(dto);
            task.setJob(obj);
            taskService.insert(task);
            obj.getTasks().add(task);
        }

        List<Job> list  = service.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Job> find(@PathVariable Integer id) {
        Job obj = service.findById(id);
        return ResponseEntity.ok().body(obj);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<Void> update(@Valid @RequestBody JobDto objDto, @PathVariable Integer id) {
        Job obj = Job.fromDTO(objDto);
        obj.setId(id);
        obj = service.update(obj);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }



}
