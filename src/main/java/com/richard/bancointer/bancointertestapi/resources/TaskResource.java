package com.richard.bancointer.bancointertestapi.resources;

import com.richard.bancointer.bancointertestapi.dto.TaskDto;
import com.richard.bancointer.bancointertestapi.model.Task;
import com.richard.bancointer.bancointertestapi.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value="/tasks")
public class TaskResource {

    @Autowired
    private TaskService service;

    @GetMapping
    public ResponseEntity<List<Task>> findByDate(@RequestParam(name = "createdAt") LocalDate createdAt) {
        List<Task> list = service.findByDate(createdAt);
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity<List<Task>> create(@Valid @RequestBody TaskDto objDto) {
        Task obj = Task.fromDTO(objDto);
        service.insert(obj);

        List<Task> list  = service.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Task> find(@PathVariable Integer id) {
        Task obj = service.findById(id);
        return ResponseEntity.ok().body(obj);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<Void> update(@Valid @RequestBody TaskDto objDto, @PathVariable Integer id) {
        Task obj = Task.fromDTO(objDto);
        obj.setId(id);
        obj = service.update(obj);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

}
