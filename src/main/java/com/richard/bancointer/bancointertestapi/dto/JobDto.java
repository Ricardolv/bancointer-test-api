package com.richard.bancointer.bancointertestapi.dto;

import com.richard.bancointer.bancointertestapi.model.ParentJob;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class JobDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    @NotBlank(message = "Preenchimento obrigatório")
    private String name;

    @NotNull(message = "Preenchimento obrigatório")
    private Boolean active;
    private List<TaskDto> tasks = new ArrayList<>();

    private ParentJobDto parentJob;
}
