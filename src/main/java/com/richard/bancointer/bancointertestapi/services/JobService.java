package com.richard.bancointer.bancointertestapi.services;

import com.richard.bancointer.bancointertestapi.model.Job;

import java.util.List;

public interface JobService {

    Job insert(Job obj);

    List<Job> findAll();

    List<Job> sortByWeight(Boolean sortByWeight);

    Job findById(Integer id);

    Job update(Job obj);

    void delete(Integer id);
}
