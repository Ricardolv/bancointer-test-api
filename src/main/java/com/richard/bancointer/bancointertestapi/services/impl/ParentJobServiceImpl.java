package com.richard.bancointer.bancointertestapi.services.impl;

import com.richard.bancointer.bancointertestapi.model.ParentJob;
import com.richard.bancointer.bancointertestapi.repository.ParentJobRepository;
import com.richard.bancointer.bancointertestapi.services.ParentJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParentJobServiceImpl implements ParentJobService {

    @Autowired
    private ParentJobRepository repo;

    @Override
    public ParentJob insert(ParentJob parentJob) {
        return repo.save(parentJob);
    }
}
