package com.richard.bancointer.bancointertestapi.services;

import com.richard.bancointer.bancointertestapi.model.Task;

import java.time.LocalDate;
import java.util.List;

public interface TaskService {

    List<Task> findByDate(LocalDate createdAt);

    Task insert(Task obj);

    List<Task> findAll();

    Task findById(Integer id);

    Task update(Task obj);

    void delete(Integer id);
}
