package com.richard.bancointer.bancointertestapi.services.impl;

import com.richard.bancointer.bancointertestapi.model.Job;
import com.richard.bancointer.bancointertestapi.model.Task;
import com.richard.bancointer.bancointertestapi.repository.JobRepository;
import com.richard.bancointer.bancointertestapi.services.JobService;
import com.richard.bancointer.bancointertestapi.services.TaskService;
import com.richard.bancointer.bancointertestapi.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private JobRepository repo;

    @Autowired
    private TaskService taskService;

    @Override
    public Job insert(Job obj) {
        obj.setId(null);
        return this.repo.save(obj);
    }

    @Override
    public List<Job> sortByWeight(Boolean sortByWeight) {

        List<Job> list = repo.findAll();

        if (sortByWeight) {
            list.stream().forEach(j -> {
                j.getTasks().sort((Task o1, Task o2) -> o2.getWeight().intValue() - o1.getWeight().intValue());
            });
        }

        return  list;
    }

    @Override
    public List<Job> findAll() {
        return this.repo.findAll();
    }

    @Override
    public Job findById(Integer id) {
        Optional<Job> obj = this.repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Job.class.getName()));
    }

    @Override
    public Job update(Job obj) {
        return this.repo.save(obj);
    }

    @Override
    public void delete(Integer id) {
        Optional<Job> jobOptional = repo.findById(id);

        if (jobOptional.isPresent()) {
            Job job = jobOptional.get();

            if (!job.getTasks().isEmpty()) {
                job.getTasks().stream().forEach(t -> {
                    taskService.delete(t.getId());
                });
            }

            this.repo.deleteById(id);
        }
    }
}
