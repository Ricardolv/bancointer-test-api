package com.richard.bancointer.bancointertestapi.services;

import com.richard.bancointer.bancointertestapi.model.ParentJob;

public interface ParentJobService {

    ParentJob insert(ParentJob parentJob);
}
