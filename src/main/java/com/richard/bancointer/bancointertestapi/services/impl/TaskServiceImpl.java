package com.richard.bancointer.bancointertestapi.services.impl;

import com.richard.bancointer.bancointertestapi.model.Task;
import com.richard.bancointer.bancointertestapi.repository.TaskRepository;
import com.richard.bancointer.bancointertestapi.services.TaskService;
import com.richard.bancointer.bancointertestapi.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository repo;

    @Override
    public Task insert(Task obj) {
        obj.setId(null);
        return this.repo.save(obj);
    }

    @Override
    public List<Task> findByDate(LocalDate createdAt) {
        return this.repo.findByCreatedAt(createdAt);
    }

    @Override
    public List<Task> findAll() {
        return this.repo.findAll();
    }

    @Override
    public Task findById(Integer id) {
        Optional<Task> obj = this.repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Task.class.getName()));
    }

    @Override
    public Task update(Task obj) {
        return this.repo.save(obj);
    }

    @Override
    public void delete(Integer id) {
        this.repo.deleteById(id);
    }
}
